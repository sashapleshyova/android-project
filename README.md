# Включение виртуализации
## На процессоре Intel
Воспользовавшись этой пошаговой инструкцией, вы сможете активировать виртуализацию (актуальна только для компьютеров, работающих на процессоре Intel):
1.	Перезагрузите компьютер и войдите в BIOS. Используйте клавиши от F2 до F12 или Delete (точная клавиша зависит от версии).
2.	Теперь вам нужно перейти в пункт «Advanced». Также он может называться «Integrated Peripherals».
3.	В нём нужно перейти в «CPU Configuration».
4.	Там необходимо найти пункт «Intel Virtualization Technology». Если данного пункта нет, то это значит, что ваш компьютер не поддерживает виртуализац 

5.	Если он есть, то обратите внимание на значение, которое стоит напротив него. Должно быть «Enable». Если там другое значение, то выберите данный пункт при помощи клавиш со стрелочками и нажмите Enter. Появится меню, где нужно выбрать корректное значение.
6.	Теперь можно сохранить изменения и выйти из BIOS с помощью пункта «Save & Exit» или клавиши F10.


## На процессоре AMD
Пошаговая инструкция выглядит в этом случае похожим образом:
1.	Войдите в BIOS.
2.	Перейдите в «Advanced», а оттуда в «CPU Configuration».
3.	Там обратите внимание на пункт «SVM Mode». Если напротив него стоит «Disabled», то вам нужно поставить «Enable» или «Auto». Значение меняется по аналогии с предыдущей инструкцией 

4.	Сохраните изменения и выйдите из BIOS



# Полезные ссылки:

### Java SE Development Kit 
https://www.oracle.com/technetwork/java/javase/downloads/2133151

### Android Studio
https://developer.android.com/studio/

### Базовые элементы в Android разработке
https://www.youtube.com/watch?v=gYylK8NudWA&list=PL0lO_mIqDDFW13-lP3IgK9lZoM1M-oPl4

### Урок про кастомный  ListView
https://www.youtube.com/watch?v=s9cypFHGaSk

### Урок по созданию базы данных
http://developer.alexanderklimov.ru/android/sqlite/cathouse2.php

### Полезный сайт про Android разработку, java, базу данных SQLite
http://developer.alexanderklimov.ru/

### Официальная документация
https://developer.android.com/docs/

### Работа с постоянным хранилищем SharedPreferences
https://www.tutorialspoint.com/android/android_session_management.htm
http://developer.alexanderklimov.ru/android/theory/sharedpreferences.php

