package com.example.user.animation;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout = (FrameLayout)findViewById(R.id.back);
        imageView = (ImageView)findViewById(R.id.rect);

        int mas[]={Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW};

        int i=0;
        ObjectAnimator objectAnimator = new ObjectAnimator();
        objectAnimator.setDuration(1000).setTarget(frameLayout);
        objectAnimator.setPropertyName("backgroundColor");
        objectAnimator.setIntValues(mas);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.setRepeatMode(ValueAnimator.RESTART);
        objectAnimator.start();

    }
}
