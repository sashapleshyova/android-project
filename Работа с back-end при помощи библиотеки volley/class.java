
package com.plescheva.aleksandra;


import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {

    Button stop, start;
    private TextView mTextViewResult;
    private RequestQueue mQueue;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextViewResult = findViewById(R.id.text_view_result);
        Button buttonParse = (Button) findViewById(R.id.button_parse);

        mQueue = Volley.newRequestQueue(this);

        buttonParse.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        jsonParse();
                    }
                }
        );

    }


    //прмер бэка
    // http://api.myjson.com/bins/kp9wz


    private void jsonParse() {
        String url = "http://localhost:1337/public/dataBase.json";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("prepods");
                            JSONObject emloyee = jsonArray.getJSONObject(0);
                            String str = emloyee.getString("prepodId");


                            mTextViewResult.append(String.valueOf(str));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

//        private void jsonParse(){
//        String url = "http://192.168.2.9:3000/userAvtor";
//            StringRequest strReq =  new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    Toast.makeText(getApplication(), response, Toast.LENGTH_LONG).show();
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(MainActivity.this, error+"", Toast.LENGTH_LONG).show();
//                }
//            })
//            {
//                @Override
//                protected Map <String, String> getParams() throws AuthFailureError
//                {
//                    Map <String, String> params = new HashMap<String, String>();
//                    //params.put("Email","ss@q.ru");
//                    params.put("Name","ss");
//                    params.put("Pass","10299");
//                    return params;
//            }
//            };
//
//            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            requestQueue.add(strReq);
//
//    }

}
