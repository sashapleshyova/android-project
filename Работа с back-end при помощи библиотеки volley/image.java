    private void sendImage() {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        ImageRequest imageRequest = new ImageRequest("https://storge.pic2.me/upload/360/5692758195371.jpg", new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                Toast.makeText(getActivity(), String.valueOf(response), Toast.LENGTH_LONG).show();
                ((ImageView) getActivity().findViewById(R.id.yourPhoto)).setImageBitmap(response);
            }
        },0,0, null,
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }
        );
        requestQueue.add(imageRequest);

    }